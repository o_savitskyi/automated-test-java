# README #

### This repository is for automated test for login system of *accounts.google.com* ###


### How can I use it? ###

* Download this repository. 
* Unzip files to any suitable directory.
* open this directory in IDE or terminal.
* find file **config.properties** in */target/test-classes/* and fill your login and password.
* Dependencies:
    * You should have **Maven** to run this test.
    * You should have **Firefox v38.0.**  You can get it on [link](https://www.mozilla.org/firefox/new/).
* In test was used:
    * selenium server standalone v2.45.0.
    * jUnit v4.12
* To run test use command `mvn test` in terminal.